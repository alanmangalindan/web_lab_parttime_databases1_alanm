drop table if exists articles;

create table articles (
	id int,
	title varchar(50),
	text varchar(250),
	primary key (id)
);

insert into articles values
	(0, 'Edmund Hillary', '<p><strong>Sir Edmund Percival Hillary</strong> KG ONZ KBE (20 July 1919 – 11 January 2008) was a New Zealand mountaineer, explorer and philanthropist. On 29 May 1953, Hillary and Nepalese Sherpa mountaineer Tenzing Norgay became the first climbers to reach the summit of Mount Everest. They were part of the ninth British expedition to Everest, led by John Hunt. Hillary was named by Time as one of the 100 most influential people of the 20th century. </p>'),
	(1, 'Apirana Ngata', '<p><strong>Sir Āpirana Turupa Ngata</strong> (3 July 1874 – 14 July 1950) was a prominent New Zealand politician and lawyer. He has often been described as the foremost Māori politician to have ever served in Parliament, and is also known for his work in promoting and protecting Māori culture and language.</p><hr />'),
	(2, 'Ernest Rutherford', '<p><strong>Ernest Rutherford, 1st Baron Rutherford of Nelson</strong>, OM, FRS (30 August 1871 – 19 October 1937) was a New Zealand-born British physicist who became known as the father of nuclear physics. Encyclopædia Britannica considers him to be the greatest experimentalist since Michael Faraday (1791–1867).</p> <hr />');
