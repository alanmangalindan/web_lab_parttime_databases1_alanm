--1. Delete a row from one table.
--insert into VideoMembers values ( 'Kate Sheppard', 'female', 1847, '1930', 1000 );
--delete from VideoMembers where name = 'Kate Sheppard';

--2. Delete a column from the same table.
--alter table VideoMembers drop column joined;
--PRAGMA foreign_keys=off;

--BEGIN TRANSACTION;

--ALTER TABLE VideoMembers RENAME TO _VideoMembers_old;

CREATE TABLE VideoMembers
( 	name varchar(50),
	gender varchar(6),
	year_born int,
	num_hires int,
	primary key (name)
);

INSERT INTO VideoMembers (name, gender, year_born, num_hires)
  SELECT name, gender, year_born, num_hires
  FROM _VideoMembers_old;

COMMIT;

PRAGMA foreign_keys=on;


--3. Delete the entire table (and then restore it by executing the corresponding SQL file again).
--drop table if exists movies;

