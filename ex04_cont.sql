--4. Change a value that is a string.
--update VideoMembers
--set name = 'Anika M'
--where name = 'Anika Moa';

--update VideoMembers
--set name = 'Anika Moa'
--where year_born = '1980';

--update VideoMembers
--set year_born = 1980
--where year_born = '1996';

--5. Change a value that is a numeric value.
--update VideoMembers
--set year_born = 1996
--where name = 'Lorde';

--6. Change a value that is a primary key.
--update VideoMembers
--set name = 'Ella Yelich-O"Connor'
--where name = 'Lorde';
update VideoMembers
set name = 'Lorde'
where name = 'Ella Yelich-O''Connor';

--delete from VideoMembers where name = 'Russell Crowe';

