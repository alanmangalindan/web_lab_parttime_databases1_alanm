--1. All information about all the members of the video store.
select * from VideoMembers;

--2. Everything about all the members of the video store, except the number of video hires they have.
select name, gender, year_born, joined from _VideoMembers_old;

--3. All the titles to the articles that have been written.
select title from articles;

--4. All the directors of the movies the video store has (without repeating any names).
select distinct director, count(*) from movies
group by director;

--5. All the video titles that rent for $2 or less per week.
--alter table movies alter column weekly_rate int;
select * from movies
where weekly_rate <= 2;

--6. A sorted list of all the usernames that have been registered.
select username from users
order by username asc;

--7. All the usernames where the user’s first name starts with the letters ‘Pete’.
select username from users
where firstname like 'Pete%';

--8. All the usernames where the user’s first name or last name starts with the letters ‘Pete’.
select username from users
where firstname like 'Pete%'
or lastname like 'Pete%';