drop table if exists users;

create table users (
	firstname varchar(30),
	lastname varchar(30),
	username varchar(20),
	email varchar(40),
	primary key (username)
);

insert into users values
	('Peter', 'Jackson', 'peterjackson', 'p.jackson@email.biz.org'),
	('Pete', 'Sampras', 'petesampras', 'p.sampras@email.biz.org'),
	('Jack', 'Peterson', 'jackpeterson', 'j.peterson@email.biz.org'),
	--('Jack', 'Johnson', 'jackpeterson', 'j.peterson@email.biz.org'),
	('Jack', 'Johnson', 'jackjohnson', 'j.peterson@email.biz.org');